// StageAccount script. Use Godot UI elements to demonstrate PlayFab integration.
using Godot;
using System;

public class StageAccount : Control
{

    private PlayFabSessionManager _sessionManager = new PlayFabSessionManager();

    private string _userEmail;
    private string _userName;
    private string _userPass;
    private string _userConfirmPass;
    private Label _lblStatus;

    public override void _Ready()
    {
        GetNode<Panel>("PnlSettings").Hide();
        GetNode<Panel>("PnlSettings/PnlAccounts").Hide();
        GetNode<Panel>("PnlSettings/PnlAccounts/PnlRegister").Show();
        GetNode<Panel>("PnlSettings/PnlAccounts/PnlLogin").Hide();
        GetNode<Button>("PnlSettings/PnlAccounts/BtnRegister").Disabled = true;
        GetNode<Button>("PnlSettings/PnlAccounts/BtnLogin").Disabled = false;
        _lblStatus = GetNode<Label>("PnlSettings/LblStatus");
        _lblStatus.Text = "";

        // Subscribe events
        _sessionManager.LoggedInStatusChanged+=this.OnLoggedInStatusChanged;
        _sessionManager.LogInErrorThrown+=this.OnLoginErrorThrown;
        _sessionManager.LogInSucceeded+=this.OnLoginSucceeded;

        // Initialise PlayFab
        _sessionManager.Start();
        // Log in using device ID
        _sessionManager.StartDeviceLogin();
    }
    

	private void _on_BtnPlay_pressed()
	{
	    GetNode<Button>("Panel/BtnPlay").Disabled = true;
        GetNode<AcceptDialog>("PopNotImplemented").Show();
	}

    private void _on_AcceptDialog_confirmed()
    {
	    GetNode<Button>("Panel/BtnPlay").Disabled = false;

    }

    private void _on_BtnSettings_pressed()
    {
        GetNode<Panel>("PnlSettings").Show();
        GetNode<Button>("PnlGameInfo/BtnUpdate").Disabled = true;
        _on_BtnAccounts_pressed();
    }

    private void _on_Panel_pressed()
    {
        GetNode<Panel>("PnlSettings").Hide();
        GetNode<Button>("PnlGameInfo/BtnUpdate").Disabled = false;
    }

    private void OnLoggedInStatusChanged(int loginStatus, string userName)
    {
        if (userName == null)
            loginStatus = 0;
        GD.Print("LoggedInStatusChange event invoked");
        string statusText = "Guest";
        switch (loginStatus)
        {
            case 0:
                break;
            case 1:
                statusText = userName;
                break;
        }
        GetNode<AnimationPlayer>("AnimLogIn").Stop();
        GetNode<Sprite>("SpriteLogIn").SetRotationDegrees(0);
        SetLoggedInStatusText(string.Format("Logged in as {0}",statusText));
    }

    private void SetLoggedInStatusText(string text)
    {
        GetNode<Label>("LblLoggedInAs").Text = text;
    }

    private void _on_BtnAccounts_pressed()
    {
        GetNode<Panel>("PnlSettings/PnlAccounts").Show();
        GetNode<Button>("PnlSettings/BtnAccounts").Disabled = true;
        _on_BtnRegister_pressed();
    }

    private void _on_BtnRegister_pressed()
    {
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlRegister").Show();
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlLogin").Hide();
            GetNode<Button>("PnlSettings/PnlAccounts/BtnRegister").Disabled = true;
            GetNode<Button>("PnlSettings/PnlAccounts/BtnLogin").Disabled = false;
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlData").Hide();
            GetNode<Button>("PnlSettings/PnlAccounts/BtnData").Disabled = false;
    }


    private void _on_BtnLogin_pressed()
    {
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlRegister").Hide();
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlLogin").Show();
            GetNode<Button>("PnlSettings/PnlAccounts/BtnRegister").Disabled = false;
            GetNode<Button>("PnlSettings/PnlAccounts/BtnLogin").Disabled = true;
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlData").Hide();
            GetNode<Button>("PnlSettings/PnlAccounts/BtnData").Disabled = false;
    }

    private void _on_BtnData_pressed()
    {
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlRegister").Hide();
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlLogin").Hide();
            GetNode<Button>("PnlSettings/PnlAccounts/BtnRegister").Disabled = false;
            GetNode<Button>("PnlSettings/PnlAccounts/BtnLogin").Disabled = false;
            GetNode<Panel>("PnlSettings/PnlAccounts/PnlData").Show();
            GetNode<Button>("PnlSettings/PnlAccounts/BtnData").Disabled = true;
    }

    private void _on_LEdUserLogin_text_changed(String text)
    {
        _userName = text;
    }


    private void _on_LEdPassLogin_text_changed(String text)
    {
        _userPass = text;
    }


    private void _on_BtnLoginSubmit_pressed()
    {
        _sessionManager.StartLogin(_userName, _userPass);
    }


    private void _on_LEdPwConfirmReg_text_changed(String text)
    {
        _userConfirmPass = text;
    }


    private void _on_LEdPassReg_text_changed(String text)
    {
        _userPass = text;
    }


    private void _on_LEdUserReg_text_changed(String text)
    {
        _userName = text;
    }


    private void _on_LEdEmailReg_text_changed(String text)
    {
        _userEmail = text;
    }

    private void _on_BtnRegSubmit_pressed()
    {
        if (_userPass != _userConfirmPass)
        {
            _lblStatus.SetText("Error: passwords do not match.");
            return;
        }
        _sessionManager.LinkToNewAccount(_userName, _userEmail, _userPass);
    }

    private void OnLoginErrorThrown(string errorText)
    {
        _lblStatus.Text = errorText;
    }

    private void OnLoginSucceeded()
    {
        _lblStatus.SetText("Success!");
    }


    private void _on_BtnForgotPw_pressed()
    {
        _sessionManager.SendAccountRecoveryEmail(_userName);
    }

    private string _keyData;
    private string _valueData;
    private string _getsetDataUsername;

    private void _on_LEdKey_text_changed(String text)
    {
        _keyData = text;
    }


    private void _on_LEdValue_text_changed(String text)
    {
        _valueData = text;
    }


    private void _on_LblEdUser_text_changed(String text)
    {
        _getsetDataUsername = text;
    }


    private void _on_BtnGetData_pressed()
    {
        _sessionManager.GetPlayerInternalData(_getsetDataUsername, _keyData);
    }


    private void _on_BtnSubmitData_pressed()
    {
        _sessionManager.SetPlayerInternalData(_getsetDataUsername, _keyData, _valueData);
    }

    // FOR TESTING (should never have secret key in client)
    private string _titleID;
    private string _secretKey;

    private void _on_LEdTitleID_text_changed(String text)
    {
        _titleID = text;
    }


    private void _on_LEdSecretKey_text_changed(String text)
    {
        _secretKey = text;
    }


    private void _on_BtnUpdate_pressed()
    {
        _sessionManager.UpdateGameInfo(_titleID, _secretKey);
        // need to reinitialise with new title ID:
        // Initialise PlayFab
        _sessionManager.Start();
        // Log in using device ID
        _sessionManager.StartDeviceLogin();
    }

}


