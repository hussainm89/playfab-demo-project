// PlayFabSessionManager script. Manage authentication / user sessions.
// In here we demonstrate:
// - Initialise playfab with title ID.
// - Login/register with device ID or username/password.
// - Link account to device
// - Set and retrieve internal player data (player facing data is easier but I wanted to do this to see how its done)

// Note we access server/admin API here for demo purposes. Should never be done in a real game (in client)
// Usually goes like: request -> task(request) -> task.ContinueWith(method to link with which handles error/result)

// Other things to think about:
// - To be GDPR compliant we should let player export his/her private data and change/delete his/her data
// - Allowing user to connect with other accounts e.g. google, facebook
// - Integrate platform-specific SDKs for in app purchases and linking this with user account. E.g. decide on target
//  platforms and integrate android specific sdk or ios sdk and link iaps with player accounts
// - Leaderboards (seems easy though)

using PlayFab;
using PlayFab.ClientModels;
using System.Threading.Tasks;
using Godot;

public class PlayFabSessionManager
{

    public delegate void LoggedInStatusChangedDelegate(int loginStatus, string username = null);
    public event LoggedInStatusChangedDelegate LoggedInStatusChanged;
    public delegate void LogInErrorThrownDelegate(string errorText);
    public event LogInErrorThrownDelegate LogInErrorThrown;
    public delegate void LogInSucceededDelegate();
    public event LogInSucceededDelegate LogInSucceeded;

    public void Start()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
        {
            // Get the title ID from Settings -> API features
            PlayFabSettings.staticSettings.TitleId = _titleID;
        }
    }
    
    #region device login
    public void StartDeviceLogin()
    {
        LoginWithCustomIDRequest requestCustom = new LoginWithCustomIDRequest { CustomId = OS.GetUniqueId(), CreateAccount = true };
        
        Task<PlayFabResult<LoginResult>> customLoginTask = PlayFabClientAPI.LoginWithCustomIDAsync(requestCustom);

        customLoginTask.ContinueWith(OnDeviceLoginComplete);
    }


    private void OnDeviceLoginComplete(Task<PlayFabResult<LoginResult>> taskResult)
    {
        PlayFabError apiError = taskResult.Result.Error;
        LoginResult apiResult = taskResult.Result.Result;

        if (apiError != null)
        {
            OnDeviceLoginFailure(apiError);
        }
        else if (apiResult != null)
        {
            OnDeviceLoginSuccess();
        }
    }


    private void OnDeviceLoginFailure(PlayFabError apiError)
    {
        GD.Print("Here's some debug information:");
        GD.Print(PlayFabUtil.GenerateErrorReport(apiError));
    }

    private void OnDeviceLoginSuccess()
    {
        GD.Print("device logged in");
        var requestAccountInfo = new GetAccountInfoRequest();
        var accountInfoTask = PlayFabClientAPI.GetAccountInfoAsync(requestAccountInfo);
        accountInfoTask.ContinueWith(OnAccountInfoRetrieved);

    }

    private void OnAccountInfoRetrieved(Task<PlayFabResult<GetAccountInfoResult>> accountInfoResult)
    {
        PlayFabError apiError = accountInfoResult.Result.Error;
        GetAccountInfoResult apiResult = accountInfoResult.Result.Result;

        if (apiError != null)
        {
            LoggedInStatusChanged?.Invoke(0);
        }
        else if (apiResult != null)
        {
            string userName = apiResult.AccountInfo.Username;
            LoggedInStatusChanged?.Invoke(1, userName);
        }
    }

    #endregion

    #region register account
    public void LinkToNewAccount(string userName, string userEmail, string userPassword)
    {
        AddUsernamePasswordRequest addUserRequest = new AddUsernamePasswordRequest { Username = userName, Password = userPassword, Email = userEmail };
        Task<PlayFabResult<AddUsernamePasswordResult>> addUserTask = PlayFabClientAPI.AddUsernamePasswordAsync(addUserRequest);
        addUserTask.ContinueWith(OnAddUserComplete);
    }

    private void OnAddUserComplete(Task<PlayFabResult<AddUsernamePasswordResult>> addUserResult)
    {
        PlayFabError apiError = addUserResult.Result.Error;
        AddUsernamePasswordResult apiResult = addUserResult.Result.Result;

        if (apiError != null)
        {
            GD.Print("Here's some debug information:");
            GD.Print(PlayFabUtil.GenerateErrorReport(apiError));
            LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        }
        else if (apiResult != null)
        {
            LogInSucceeded?.Invoke();
            OnDeviceLoginSuccess();
        }

    }
    #endregion

    #region link account


    public void StartLogin(string userName, string userPassword)
    {

        LoginWithPlayFabRequest request =  new LoginWithPlayFabRequest { Username = userName, Password = userPassword };
        Task<PlayFabResult<LoginResult>> loginTask = PlayFabClientAPI.LoginWithPlayFabAsync(request);

        loginTask.ContinueWith(OnLoginComplete);
    }

    private void OnLoginComplete(Task<PlayFabResult<LoginResult>> taskResult)
    {
        PlayFabError apiError = taskResult.Result.Error;
        LoginResult apiResult = taskResult.Result.Result;

        if (apiError != null)
        {
            OnLoginFailure(apiError);
        }
        else if (apiResult != null)
        {
            OnLoginSuccess();
        }
    }

    private void OnLoginFailure(PlayFabError apiError)
    {

        GD.Print("Here's some debug information:");
        GD.Print(PlayFabUtil.GenerateErrorReport(apiError));
        LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        
    }

    private void OnLoginSuccess()
    {
        LinkToExistingAccount();
    }

    public void LinkToExistingAccount()
    {
        
        LinkCustomIDRequest linkIDRequest = new LinkCustomIDRequest { CustomId = OS.GetUniqueId(), ForceLink = true };
        Task<PlayFabResult<LinkCustomIDResult>> linkIDTask = PlayFabClientAPI.LinkCustomIDAsync(linkIDRequest);
        linkIDTask.ContinueWith(OnLinkIDComplete);
    }

    private void OnLinkIDComplete(Task<PlayFabResult<LinkCustomIDResult>> linkIDResult)
    {

        PlayFabError apiError = linkIDResult.Result.Error;
        LinkCustomIDResult apiResult = linkIDResult.Result.Result;

        if (apiError != null)
        {
            GD.Print("Here's some debug information:");
            GD.Print(PlayFabUtil.GenerateErrorReport(apiError));
            LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        }
        else if (apiResult != null)
        {
            LogInSucceeded?.Invoke();
            OnDeviceLoginSuccess(); // get the player username and show it as logged in
        }
    }
    #endregion

    #region recover password

    // NOTE: this should be run by admin / on server. NOT ON CLIENT. secret key should never be on client.
    // should probably be encrypted on the server. remember it can be used to access user private data
    // here just for testing purposes. in a real game admin code would not be on client
    // the client would send an rpc to the server requesting email sent. 
    // the server would start a timer to prevent email spam / abuse, and a counter to limit number of resets / day, and send using admin api
    private string _secretKey;
    private string _titleID;

    // for testing purposes
    public void UpdateGameInfo(string titleID, string secretKey)
    {
        _titleID = titleID;
        _secretKey = secretKey;
    }


    public void SendAccountRecoveryEmail(string userName)
    {
        // OOOPS. shouldnt run this on client or give client secret key.
        PlayFabSettings.staticSettings.DeveloperSecretKey = _secretKey;

        // Sends email to account linked with user's unique ID
        var requestEmailAdd = new PlayFab.AdminModels.LookupUserAccountInfoRequest
        {
            Username = userName
        };
        var emailAddTask = PlayFab.PlayFabAdminAPI.GetUserAccountInfoAsync(requestEmailAdd);
        emailAddTask.ContinueWith(OnEmailAddRetrieved);

    }

    private void OnEmailAddRetrieved(Task<PlayFabResult<PlayFab.AdminModels.LookupUserAccountInfoResult>> accountInfoResult)
    {
        PlayFabError apiError = accountInfoResult.Result.Error;
        PlayFab.AdminModels.LookupUserAccountInfoResult apiResult = accountInfoResult.Result.Result;

        if (apiError != null)
        {
            LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        }

        else if (apiResult != null)
        {
            GD.Print("we got a result retrieiving email add");
            string emailAdd = apiResult.UserInfo.PrivateInfo.Email;
            GD.Print("EMAIL ", emailAdd);

            var request = new PlayFab.ClientModels.SendAccountRecoveryEmailRequest
            {
                TitleId = _titleID,
                Email = emailAdd

            };

            var sendEmailTask = PlayFabClientAPI.SendAccountRecoveryEmailAsync(request);

            sendEmailTask.ContinueWith(OnSendRecoveryEmailComplete);
        }
    }

    private void OnSendRecoveryEmailComplete(Task<PlayFabResult<PlayFab.ClientModels.SendAccountRecoveryEmailResult>> taskResult)
    {
        PlayFabError apiError = taskResult.Result.Error;
        var apiResult = taskResult.Result.Result;

        if (apiError != null)
        {
            LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        }
        else if (apiResult != null)
        {
            LogInErrorThrown?.Invoke("Email sent.");
        }
    }
    #endregion

    # region set internal player data
    // again this should be done by admin / server only. here for demo purposes
    
    private string _keyData;
    private string _valueData;

    public void SetPlayerInternalData(string userName, string key, string value)
    {

        // OOOPS. shouldnt run this on client or give client secret key.
        PlayFabSettings.staticSettings.DeveloperSecretKey = _secretKey;

        _keyData = key;
        _valueData = value;

        var requestPlayFabID = new PlayFab.AdminModels.LookupUserAccountInfoRequest
        {
            Username = userName
        };

        var playFabIDTask = PlayFab.PlayFabAdminAPI.GetUserAccountInfoAsync(requestPlayFabID);
        playFabIDTask.ContinueWith(OnPlayFabIDRetrieved);
    }


    private void OnPlayFabIDRetrieved(Task<PlayFabResult<PlayFab.AdminModels.LookupUserAccountInfoResult>> accountInfoResult)
    {
        PlayFabError apiError = accountInfoResult.Result.Error;
        PlayFab.AdminModels.LookupUserAccountInfoResult apiResult = accountInfoResult.Result.Result;
        
        if (apiError != null)
        {
            LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        }

        else if (apiResult != null)
        {
            string playFabID = apiResult.UserInfo.PlayFabId;
            var requestUpdateUserInternalData = new PlayFab.ServerModels.UpdateUserInternalDataRequest
            {
                PlayFabId = playFabID
            };
            if (requestUpdateUserInternalData.Data == null)
            {
                requestUpdateUserInternalData.Data = new System.Collections.Generic.Dictionary<string, string>();
            }
            requestUpdateUserInternalData.Data.Add(_keyData, _valueData);
            var updateUserDataTask = PlayFab.PlayFabServerAPI.UpdateUserInternalDataAsync(requestUpdateUserInternalData);
            updateUserDataTask.ContinueWith(OnUserDataUpdated);
        }
    }

    private void OnUserDataUpdated(Task<PlayFabResult<PlayFab.ServerModels.UpdateUserDataResult>> updateUserDataResult)
    {
        PlayFabError apiError = updateUserDataResult.Result.Error;
        PlayFab.ServerModels.UpdateUserDataResult apiResult = updateUserDataResult.Result.Result;
        
        if (apiError != null)
        {
            LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        }

        else if (apiResult != null)
        {
            LogInErrorThrown?.Invoke("User data updated");
        }
    }

    #endregion

    #region get player internal data
    // again this should be done by admin / server only. here for demo purposes
    
    public void GetPlayerInternalData(string userName, string key)
    {

        // OOOPS. shouldnt run this on client or give client secret key.
        PlayFabSettings.staticSettings.DeveloperSecretKey = _secretKey;

        _keyData = key;

        var requestPlayFabID = new PlayFab.AdminModels.LookupUserAccountInfoRequest
        {
            Username = userName
        };

        var playFabIDTask = PlayFab.PlayFabAdminAPI.GetUserAccountInfoAsync(requestPlayFabID);
        playFabIDTask.ContinueWith(GetPlayerDataFromID);
    }


    private void GetPlayerDataFromID(Task<PlayFabResult<PlayFab.AdminModels.LookupUserAccountInfoResult>> accountInfoResult)
    {
        PlayFabError apiError = accountInfoResult.Result.Error;
        PlayFab.AdminModels.LookupUserAccountInfoResult apiResult = accountInfoResult.Result.Result;
        
        if (apiError != null)
        {
            LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        }

        else if (apiResult != null)
        {
            string playFabID = apiResult.UserInfo.PlayFabId;

            var requestGetUserInternalData = new PlayFab.ServerModels.GetUserDataRequest
            {
                PlayFabId = playFabID
            };

            var getUserInternalDataTask = PlayFab.PlayFabServerAPI.GetUserInternalDataAsync(requestGetUserInternalData);
            getUserInternalDataTask.ContinueWith(OnUserDataRetrieved);
        }
    }

    private void OnUserDataRetrieved(Task<PlayFabResult<PlayFab.ServerModels.GetUserDataResult>> userDataResult)
    {
        PlayFabError apiError = userDataResult.Result.Error;
        PlayFab.ServerModels.GetUserDataResult apiResult = userDataResult.Result.Result;
        
        if (apiError != null)
        {
            LogInErrorThrown?.Invoke(PlayFabUtil.GenerateErrorReport(apiError));
        }

        else if (apiResult != null)
        {
            LogInErrorThrown?.Invoke(string.Format("Value for specified key: {0}", apiResult.Data[_keyData].Value));
        }
    }

    #endregion

}
